# Rhomb.io Project for VS Code

This is a [VS Code](https://code.visualstudio.com) project pre-configured to work with any [Rhomb.io](https://rhomb.io) device as the defacto development environment.

The `.vscode` folder contains local settings and extensions recommendations to follow the project code style and other guidance that help you to focus on code and not on configuring the editor.

## Install & Usage

* Download the project from git@gitlab.com:rhombio/all_rhombio_micros.git
* Rename the file platformio.sample.ini to platformio.ini. If you intend to collaborate with this project please keep a copy of platformio.sample.ini on the root folder.
* Inside platformio.ini define the variable 'default_envs = ' to specify which micro you are going to work with.
* You may uncomment some of the lines that begin with semicolon ";" to  use that code as you see fit.

## Development

This project is open source, you are welcome to send issues and pull requests.
